<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
session_start();


$client_id = '71f70af8f51b4817b9d6fde75e431046'; // test.str-k.ru

$client_secret = '5251be0b005b4ac0a48946a05f0a7e10'; // test.str-k.ru

function get_data($oauth_token, $url)
{
  $output = '';
  if (!empty($oauth_token))
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$oauth_token));
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_NOBODY, 0);
    $output = curl_exec($ch);
    curl_close($ch);

    $output = json_decode($output, true);
  } 

  return $output;
}


$oauth_token = @$_SESSION['oauth_token'];
if (empty($oauth_token) AND empty($_GET['code']))
{
  echo "<a href='https://oauth.yandex.ru/authorize?response_type=code&client_id=".$client_id."'>Получить доступ</a>";
  exit;
}

if (!empty($_GET['code']))
{
  $query = array(
      'grant_type' => 'authorization_code',
      'code' => $_GET['code'],
      'client_id' => $client_id,
      'client_secret' => $client_secret
  );
  $query = http_build_query($query);


  $header = "Content-type: application/x-www-form-urlencoded";

  $opts = array('http' =>
    array(
      'method'  => 'POST',
      'header'  => $header,
      'content' => $query
    ) 
  );
  $context = stream_context_create($opts);
  $result = file_get_contents('https://oauth.yandex.ru/token', false, $context);
  $result = json_decode($result);

  $oauth_token = $result->access_token;
  $_SESSION['oauth_token'] = $oauth_token;
  header("Location: yandex.php");

}


$segments = array();
$counters = get_data($oauth_token, 'https://api-metrika.yandex.net/management/v1/counters');
$params = array();
$schedule = array();


$date1 = @$_GET['date1'];
$date2 = @$_GET['date2'];
$period = @$_GET['period'];

if (empty($period)) $period = "day";
if (empty($date1)) $date1 = "2019-04-01";
if (empty($date2)) $date2 = date('Y-m-d');

$period_options = "";
$period_arr = array('day' => 'День', 'week' => 'Неделя', 'month' => 'Месяц', 'year' => 'Год');
foreach ($period_arr as $value => $label) 
{
  $selected = "";
  if ($value == $period) $selected = " selected";
  $period_options .= "<option value='".$value."'".$selected.">".$label."</option>";
}


$output = "";

foreach ($counters['counters'] as $counter)
{
	$segments[$counter['id']] = get_data($oauth_token, 'https://api-metrika.yandex.net/management/v1/counter/'.$counter['id'].'/segments/');

	if (count($segments[$counter['id']]['segments']) != 0) 
	{
		foreach ($segments[$counter['id']]['segments'] as $segment => $arr)	if (!empty($arr['name']) AND $arr['name'] == 'Брендовые запросы') $segments[$counter['id']] = @$arr['expression'];
	}

	$params = get_data($oauth_token, "https://api-metrika.yandex.net/stat/v1/data/bytime?preset=sources_search_phrases&group=".$period."&filters=".urlencode($segments[$counter['id']])."&ids=".$counter['id']."&date1=".$date1."&date2=".$date2);

	foreach ($params['time_intervals'] as $day => $date) $schedule[date('d/m/Y', strtotime($date[0]))][$counter['id']] = $params['totals'][0][$day];

	$output .= "'".$counter['name']."',";

}


$output = "['Дата',".substr($output, 0, -1).",'Итог'],";
foreach ($schedule as $date => $arr)
{
	$values = "";
	$total = 0;
	foreach ($arr as $counter => $value) 
	{
		$values .= $value.",";
		$total += $value;
	}
	$output .= "['".$date."',".substr($values, 0, -1).",".$total."],";
}
$output = substr($output, 0, -1);


?>

<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([<?php echo $output; ?>]);

        var options = {
          title: 'Тестовый график',
          hAxis: {title: 'Дата',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
  	<form action='yandex.php' method='get'>
  		<div><label>Дата от </label><input type='text' name='date1' value='<?php echo $date1; ?>' style='width:200px' /></div>
  		<div>&nbsp;</div>
  		<div><label>Дата до </label><input type='text' name='date2' value='<?php echo $date2; ?>' style='width:200px' /></div>
  		<div>&nbsp;</div>
      <div><label>Период </label><select name='period' style='width:200px'><?php echo $period_options; ?></select></div>
      <div>&nbsp;</div>
  		<div><input type='submit' value='Отправить' /></div>
  	</form>
    <div id="chart_div" style="width: 60%; height: 350px;"></div>
  </body>
</html>

